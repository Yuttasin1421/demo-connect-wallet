import logo from "./logo.svg";
import "./App.css";
import WalletConnect from "@walletconnect/client";
import QRCodeModal from "@walletconnect/qrcode-modal";

function App() {
  let isConnect = false;
  let walletId = "1234";
  let payload_;
  let accounts = [];
  window.global = window;
  // @ts-ignore
  window.Buffer = window.Buffer || require("buffer").Buffer;
  const connector = new WalletConnect({
    bridge: "https://bridge.walletconnect.org",
    qrcodeModal: QRCodeModal,
  });

  // Check if connection is established
  if (!connector.connected) {
    // Create a dialogue
    console.log("start create session");
    connector.createSession();
  } else {
    return <div>{connector.accounts[0]}</div>;
  }

  // Subscribe to connection events
  connector.on("connect", (error, payload) => {
    console.log("connected");
    console.log(payload);
    payload_ = payload;
    isConnect = true;
    if (error) {
      throw error;
    }
    // After the connection is successful, the wallet account and chain ID will be returned
    const { accounts, chainId } = payload.params[0];
    console.log("acc", payload.params[0].accounts[0]);
    window.location.reload();
  });

  connector.on("session_update", (error, payload) => {
    console.log("session_updated");
    if (error) {
      throw error;
    }
    const { accounts, chainId } = payload.params[0];
    walletId = payload.params[0].accounts[0];
    window.location.reload();
  });

  connector.on("disconnect", (error, payload) => {
    console.log("disconnected");
    if (error) {
      throw error;
    }
  });
  console.log(payload_);
  console.log("accounts", accounts);
  return <div>error</div>;
}

export default App;
